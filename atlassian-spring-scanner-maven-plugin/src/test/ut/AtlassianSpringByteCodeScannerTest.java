package ut;

import com.atlassian.plugin.spring.scanner.core.AtlassianSpringByteCodeScanner;
import com.atlassian.plugin.spring.scanner.core.ByteCodeScannerConfiguration;
import ut.testdata.DeclaredComponent;
import org.junit.*;
import static org.junit.Assert.*;

import java.net.URL;
import java.util.HashSet;
import java.util.Set;

public class AtlassianSpringByteCodeScannerTest
{
    /**
     * Create a scanner config to scan all of the classes in testdata package.
     *
     * @param permitDuplicateImports
     * @return
     */
    private ByteCodeScannerConfiguration createScannerConfiguration(boolean permitDuplicateImports) {
        Set<URL> scanPaths = new HashSet<>();
        scanPaths.add(DeclaredComponent.class.getResource(""));

        return ByteCodeScannerConfiguration.builder()
                .setVerbose(false)
                .setPermitDuplicateImports(permitDuplicateImports)
                .setClassPathUrls(scanPaths)
                .build();
    }

    @Test
    public void testForErrorOnDuplicateImport() {
        ByteCodeScannerConfiguration config;
        AtlassianSpringByteCodeScanner scanner;

        // Verify failure when permitDuplicateImports==false
        config = createScannerConfiguration(false);
        scanner = new AtlassianSpringByteCodeScanner(config);
        assertEquals(1, scanner.getErrors().getErrorsEncountered().size());

        // Verify success when permitDuplicateImports==true
        config = createScannerConfiguration(true);
        scanner = new AtlassianSpringByteCodeScanner(config);
        assertEquals(0, scanner.getErrors().getErrorsEncountered().size());
    }
}
