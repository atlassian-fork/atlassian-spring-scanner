package com.atlassian.plugin.spring.scanner.core.vfs;

import java.io.File;

/**
 * A virtual file interface to abstract the differences between Javac Filer disk access and bog standard File access.
 * <p>
 * This is the factory to get virtual files from.
 */
public class VirtualFileFactory {
    private final File baseDir;

    public VirtualFileFactory(File baseDir) {
        this.baseDir = baseDir;
    }

    public VirtualFile getFile(String fileName) {
        File file = new File(baseDir, fileName);
        return new FileBasedVirtualFile(file);
    }
}
