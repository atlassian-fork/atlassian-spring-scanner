package com.atlassian.plugin.spring.scanner.runtime.impl;


import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.Map;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

public class ExportedSeviceManagerTest {
    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private BundleContext bundleContext;

    @Mock
    private Bundle bundle;

    @Mock
    private ServiceRegistration serviceRegistration;

    @Mock
    private ServiceReference serviceReference;

    @Mock
    private Map<String, Object> serviceProps;

    @InjectMocks
    private ExportedSeviceManager exportedSeviceManager;

    @Before
    public void setUp() {
        when(bundleContext.getBundle()).thenReturn(bundle);
        when(bundle.getHeaders()).thenReturn(new Hashtable<>());
        when(bundleContext.registerService(any(String[].class), any(), any())).thenReturn(serviceRegistration);
        when(serviceRegistration.getReference()).thenReturn(serviceReference);
        when(serviceReference.getPropertyKeys()).thenReturn(new String[0]);
    }

    @Test
    public void servicesAreStoredByIdentity() throws Exception {
        Object bean = new Bean();
        exportedSeviceManager.registerService(bundleContext, bean, "bean", serviceProps, Serializable.class);

        assertThat(exportedSeviceManager.hasService(bean), equalTo(true));
        exportedSeviceManager.unregisterService(bundleContext, bean);
        assertThat(exportedSeviceManager.hasService(bean), equalTo(false));
    }

    @Test
    public void servicesAreNotStoredByEquality() throws Exception {
        exportedSeviceManager.registerService(bundleContext, new EqualBean(), "bean", serviceProps, Serializable.class);

        assertThat(exportedSeviceManager.hasService(new EqualBean()), equalTo(false));
    }

    @Test
    public void registerTwiceSucceeds() throws Exception {
        Object bean = new Bean();
        exportedSeviceManager.registerService(bundleContext, bean, "bean", serviceProps, Serializable.class);
        exportedSeviceManager.registerService(bundleContext, bean, "bean", serviceProps, Serializable.class);
    }

    private static class Bean implements Serializable {

    }

    private static class EqualBean implements Serializable {
        @Override
        public int hashCode() {
            return 1;
        }

        @Override
        public boolean equals(Object obj) {
            return true;
        }
    }
}