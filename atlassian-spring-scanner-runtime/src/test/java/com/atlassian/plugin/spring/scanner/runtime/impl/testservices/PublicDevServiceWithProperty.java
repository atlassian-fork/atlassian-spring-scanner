package com.atlassian.plugin.spring.scanner.runtime.impl.testservices;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsDevService;
import com.atlassian.plugin.spring.scanner.annotation.export.ServiceProperty;
import org.springframework.stereotype.Component;

@ExportAsDevService(
        value = {Service.class},
        properties = {@ServiceProperty(key = "dev_key", value = "dev_service_with_properties_key")}
)
@Component
public class PublicDevServiceWithProperty implements Service {
    @Override
    public void a() {

    }
}
