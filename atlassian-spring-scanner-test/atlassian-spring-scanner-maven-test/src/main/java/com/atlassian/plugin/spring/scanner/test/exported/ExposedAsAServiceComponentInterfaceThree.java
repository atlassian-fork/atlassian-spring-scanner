package com.atlassian.plugin.spring.scanner.test.exported;

public interface ExposedAsAServiceComponentInterfaceThree {
    void doMoreStuff();
}
