package com.atlassian.plugin.spring.scanner.test.servlet;

import org.springframework.beans.factory.annotation.RequiredAnnotationBeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.core.SpringVersion;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Returns Spring version which product is using, so test can assert against them.
 */
public class SpringStatusServlet  extends HttpServlet {

    private final ApplicationContext parentContext;

    public SpringStatusServlet(ApplicationContext parentContext) {
        this.parentContext = parentContext;
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws IOException {
        response.setContentType("text/plain");
        String version = SpringVersion.getVersion();
        if (version == null) {
            version = determineVersionBasedOnBeanDefinition();
        }

        response.getWriter().println(version);
        response.flushBuffer();
    }

    /**
     * We don't know exact version of Spring but we know if product is using Spring 5 or not
     * @return
     */
    private String determineVersionBasedOnBeanDefinition() {
        String[] beanNames = parentContext.getBeanNamesForType(RequiredAnnotationBeanPostProcessor.class);
        if (beanNames != null && beanNames.length > 0 ) {
            return "4.2.4.RELEASE";
        } else {
            return "5.1.0";
        }
    }
}
