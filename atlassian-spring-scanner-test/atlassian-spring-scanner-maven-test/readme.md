Test plugin exercising all functionality.

To run integration tests in refapp: `mvn verify`

To run integration tests in other products: `mvn -DtestGroups=jira verify`

To run manually and attach debugger: `mvn -Dproduct=refapp -Djvm.debug.suspend=true amps:debug`

JIRA Cloud with Postgres-in-Docker:

`docker login https://docker.atlassian.io`
`mvn -Pjira-cloud,postgres84 -Djira.version=x.y.z clean verify`
`mvn -Pjira-cloud,postgres84 -Dproduct=jira -Djira.version=x.y.z -Ddocker.host.address=192.168.99.100 -DskipTests -Djvm.debug.suspend=true amps:debug`

(Note all the products are configured to appear at the same URL: "http://localhost:5990/product")

Interesting URLs (see tests):

http://localhost:5990/product/plugins/servlet/call-custom-module-type
http://localhost:5990/product/plugins/servlet/component-status?components
http://localhost:5990/product/plugins/servlet/component-status?services
http://localhost:5990/product/plugins/servlet/manage-dynamic-contexts
